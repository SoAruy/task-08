package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    private int temperature;
    private String lightning;
    private int watering;

    public GrowingTips(int temperature, String lightning, int watering) {
        this.temperature = temperature;
        this.lightning = lightning;
        this.watering = watering;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getLightning() {
        return lightning;
    }

    public void setLightning(String lightning) {
        this.lightning = lightning;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }
}
