package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.List;

import static com.epam.rd.java.basic.task8.entity.Flower.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowersDOM = domController.getFlowers();

		sortByName(flowersDOM);

		String outputXmlFile = "output.dom.xml";
		domController.createXMLFile(outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> flowersSAX = domController.getFlowers();

		sortByOrigin(flowersSAX);

		outputXmlFile = "output.sax.xml";
		saxController.createXMLFile(outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowersSTAX = staxController.getFlowers();

		sortBySoil(flowersSTAX);

		outputXmlFile = "output.stax.xml";
		staxController.createXMLFile(outputXmlFile);

	}

}
